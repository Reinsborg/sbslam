
import matplotlib.pyplot as plt
import numpy as np

class rmse_data:
	def __init__(self, traj_rmse, map_rmse,traj_std, map_std, agents, n, fails):
		self.traj_rmse = traj_rmse
		self.traj_std = traj_std
		self.map_rmse = map_rmse
		self.map_std = map_std
		self.agents = agents
		self.N = n
		self.fails = fails


singleAgent = rmse_data(2.08199, 1.039826364, 0.04145636364, 0.04277421488, 1, 10, 1)
vFormation_0deg  = rmse_data(0.2512752167, 0.985847, 0.1444154789, 0.2590550667, 3, 10, 0)
vFormation_17deg = rmse_data(0.2928661231, 0.9217961154, 0.1529638189, 0.2294715769, 3,10 ,0)
vFormation_30deg = rmse_data(0.3797646867, 0.8691460667, 0.22835438, 0.2202620756,3, 10,8)
vFormation_45deg = rmse_data(0.6608624,	1.01408016, 0.511267168, 0.5214655488, 3, 10, 13)

plt.rcParams.update({'font.size': 14})


labels = ["single", "0 deg", "17 deg", "30 deg", "45 deg"]
traj_means = [singleAgent.traj_rmse, vFormation_0deg.traj_rmse, vFormation_17deg.traj_rmse, vFormation_30deg.traj_rmse, vFormation_45deg.traj_rmse]
map_means = [singleAgent.map_rmse, vFormation_0deg.map_rmse, vFormation_17deg.map_rmse, vFormation_30deg.map_rmse, vFormation_45deg.map_rmse]
traj_stds = [singleAgent.traj_std, vFormation_0deg.traj_std, vFormation_17deg.traj_std, vFormation_30deg.traj_std, vFormation_45deg.traj_std]
map_stds = [singleAgent.map_std, vFormation_0deg.map_std, vFormation_17deg.map_std, vFormation_30deg.map_std, vFormation_45deg.map_std]

x = np.arange(len(labels))
width = 0.35

fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, traj_means, width, yerr=traj_stds, capsize=5, label="Trajectory RMSE")
rects2 = ax.bar(x + width/2, map_means, width, yerr=map_stds, capsize=5, label="Map points RMSE")

ax.grid(which='major', axis='y')
ax.set_ylabel("RMSE (m)")
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()
ax.set_axisbelow(1)


fig.tight_layout()
plt.savefig("RMSE_with_stddev.png")
plt.show()

fig2, ax2 = plt.subplots()
fails = [singleAgent.fails, vFormation_0deg.fails, vFormation_17deg.fails, vFormation_30deg.fails, vFormation_45deg.fails]
fails = np.asarray(fails, dtype=float)
fails[0] = fails[0]/10.0
fails[1:] = fails[1:]/30.0
fails = fails * 100
rec = ax2.bar(x, fails, width, color="darkred", label="Wrong loop closures")

ax2.set_ylabel("Failure rate (%)")
ax2.set_ylim([0, 100])
ax2.set_xticks(x)
ax2.set_xticklabels(labels)
ax2.legend()
ax2.grid(which='major', axis='y')
ax2.set_axisbelow(1)

fig2.tight_layout()
plt.savefig("Failed_loop_closures.png")
plt.show()

