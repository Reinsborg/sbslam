#!/usr/bin/env python2
import rospy
import message_filters
from geometry_msgs.msg import Pose, Point
import numpy as np
import sys
from sbslam_ubsar.msg import situated_coms_list, situated_coms_msg
import string

SITCOM_RANGE = 10

def Point2Array(point):

	return np.asarray([point.x, point.y, point.z])



class situated_coms:

	def __init__(self):
		rospy.init_node("situated_comminication_sim")
		topics = np.asarray(rospy.get_published_topics())
		topics = topics[:, 0]  # slice topic type away



		self.sub_topics = [x for x in topics if x.endswith("odometry_sensor1/pose")]  # find pose topics
		self.pubs = [rospy.Publisher('/' + topic.split('/')[1] + "/simulated_situated_com", situated_coms_list, queue_size=1) for topic in self.sub_topics]
		self.subs = [message_filters.Subscriber(topic, Pose) for topic in self.sub_topics]
		self.time_sync = message_filters.ApproximateTimeSynchronizer(self.subs, 10, 0.1, allow_headerless=True)
		self.time_sync.registerCallback(self.situated_com_callback)



	def situated_com_callback(self, *data):
		positions = [Point2Array(pose.position)[:-1] for pose in data]

		dist = dict()
		ang = dict()
		for a, posA in enumerate(positions):
			for b, posB in enumerate(positions[a+1:]):
				b += a+1
				diff = posB-posA
				dist[frozenset((a,b))] = np.linalg.norm(diff)
				ang[(a,b)] = np.arctan2(diff[1], diff[0])
				tmp = ang[(a,b)] + np.pi
				ang[(b,a)] = tmp if tmp < np.pi else tmp-2*np.pi

		for i, pub in enumerate(self.pubs):
			sit_com = []
			for y in range(0, len(positions)):
				if y==i:
					continue
				if dist[frozenset((i,y))] > SITCOM_RANGE:
					continue
				dist_ang_msg = situated_coms_msg()
				dist_ang_msg.dist = dist[frozenset((i,y))]
				dist_ang_msg.ang = ang[(i,y)]
				sit_com.append(dist_ang_msg)

			sitcom_list = situated_coms_list()
			sitcom_list.header.stamp = rospy.Time.now()
			sitcom_list.sit_coms = sit_com
			pub.publish(sitcom_list)




def main(args):
    node = situated_coms()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")


if __name__ == '__main__':
    print("Launching the situated communication sim")
    main(sys.argv)