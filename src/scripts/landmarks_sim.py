#!/usr/bin/env python2
import rospy
import sys

from gazebo_msgs.msg import ModelStates
from geometry_msgs.msg import Pose, Point
from sbslam_ubsar.srv import Landmarks
import numpy as np
from itertools import compress
import quaternion

FOV = np.pi #rad
RANGE = 10 #m

class landmarks_sim:
    def __init__(self):
        rospy.init_node("landmarks_sim")
        model_states = rospy.wait_for_message("/gazebo/model_states", ModelStates)
        self.model_poses = model_states.pose[1:]
        self.model_names = model_states.name[1:]
        #print("LANDMARK SIM MODELS FOUND:")
        #print(self.model_names)
        to_np_arr = lambda pose : np.asarray([pose.position.x, pose.position.y, pose.position.z])
        self.model_poses = np.asarray(list(map(to_np_arr, self.model_poses)))
        s = rospy.Service('/landmarks', Landmarks, self.calc_visible_landmarks)
        rospy.spin()
    
    def calc_visible_landmarks(self, data):
        data = data.pose
        pos = np.asarray([data.position.x, data.position.y, data.position.z])
        dir = np.quaternion(data.orientation.w, data.orientation.x, data.orientation.y, data.orientation.z)
        diff = np.subtract(self.model_poses, pos)
        dist = map(np.linalg.norm, diff)
        # select landmarks that are within range
        in_range = [x<RANGE for x in dist]
        #landmarks = self.model_poses[in_range]
        #landmarks = list(compress(diff, in_range))
        #desc = self.model_names[in_range]
        desc = list(compress(self.model_names, in_range))
        #diff = diff[in_range]
        diff = list(compress(diff, in_range))
        #dist = dist[in_range]
        dist = list(compress(dist, in_range))

        # calc azimuth between virt cam and landmarks
        diff = list(map(lambda x: np.quaternion(0, *x) , diff))
        q = [dir.conjugate() * x * dir for x in diff]

        az = [np.arctan2(p.y, p.x) for p in q]
        # select landmarks with azimuth withing FoV
        in_fov = [np.abs(x) < FOV/2 for x in az]
        #landmarks = landmarks[in_fov]
        landmarks = list(compress(q, in_fov))
        #desc = desc[in_fov]
        desc = list(compress(desc, in_fov))
        #dist = dist[in_fov]
        dist = list(compress(dist, in_fov))
        #az = az[in_fov]
        az = list(compress(az, in_fov))
        #print("VISIBLE LANDMARKS az:")
        #print(az)
        landmarks = [quaternion.quaternion.normalized(q)*dist[i] for (i,q) in enumerate(landmarks)]
        landmarks = [Point(x=q.x, y=q.y, z=q.z) for q in landmarks]
        return {'landmarks' : landmarks, 'descriptors': desc, 'azimuths': az, 'dists': dist}




def main(args):
    node = landmarks_sim()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")


if __name__ == '__main__':
    print("Launching the landmarks sim")
    main(sys.argv)