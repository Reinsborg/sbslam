import rospy
import sys


from geometry_msgs.msg import Pose, Point
from sbslam_ubsar.srv import Landmarks
import numpy as np

class fake_cam:
	def __init__(self, node_name):
		rospy.init_node("fake_cam"+node_name)

def main(args):
    node = fake_cam(node_name= args[1])
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")


if __name__ == '__main__':
    print("Launching the fake cam")
    main(sys.argv)